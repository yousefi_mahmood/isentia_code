import argparse
import spacy
import pandas as pd
import numpy as np
import nltk
from nltk.corpus import wordnet as wn
from spacy.lang.en import English
import gensim
from gensim import corpora
import pickle


def averaging(wv, words, idx):
    all_words, mean = set(), []
    for word in words:
        if isinstance(word, np.ndarray):
            mean.append(word)
        elif word in wv.vocab:
            mean.append(wv.vectors_norm[wv.vocab[word].index])
            all_words.add(wv.vocab[word].index)

    if not mean:
        print("Document in line %d is a special case: %s" %(idx+1, words))
        return np.zeros(wv.vector_size, )

    mean = gensim.matutils.unitvec(np.array(mean).mean(axis=0)).astype(np.float32)
    return mean


def averaging_docs(wv, text_list):
    return np.vstack([averaging(wv, post, idx) for idx, post in enumerate(text_list)])


def stemmer(text):
    stem = [nltk.PorterStemmer().stem(i) for i in text]
    return stem

def morphic(word):
    lemma = wn.morphy(word)
    if lemma is None:
        return word
    else:
        return lemma


def tokenize(text):
    token = []
    # basic cleaning
    pattern = r'[^a-zA-z0-9\s]'
    text = nltk.re.sub(pattern, ' ', text)

    tokens = par(text)
    # more sanitization
    for row in tokens:
        if row.orth_.isspace():
            continue
        elif row.like_url:
            token.append('URL')
        elif row.orth_.startswith('@'):
            token.append('SCREEN_NAME')
        else:
            token.append(row.lower_)
    return token


def preparation(text):
    tokens = tokenize(text)
    tokens = [token for token in tokens if len(token) > 2]
    tokens = [token for token in tokens if token not in stops]
    tokens = [morphic(token) for token in tokens]
    tokens = stemmer(tokens)
    return tokens


def data_load(file):
    text_data = []
    label = []
    with open(file, encoding="utf8") as f:
        for line in f:
            line = line.split("__label__")
            tokens = preparation(line[0])
            text_data.append(tokens)
            label.append(line[1][:-1])

    return text_data, label


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Tokenization and Feature Extraction')
    parser.add_argument("-d", "--data", required=True, help="Input data file including txt extension")
    args = vars(parser.parse_args())

    par = English()
    nltk.download('wordnet')
    nltk.download('stopwords')
    spacy.load('en')
    stops = set(nltk.corpus.stopwords.words('english'))

    print("Tokenizing data ...")
    data, label = data_load(args["data"])

    print("Saving corpus and dictionary pickles ...")

    dic = corpora.Dictionary(data)
    corpus = [dic.doc2bow(txt) for txt in data]
    pickle.dump(corpus, open('corpus.pkl', 'wb'))
    dic.save('dic.gensim')

    print("Loading word2vec pre-trained Google News ...")
    wv = gensim.models.KeyedVectors.load_word2vec_format("GoogleNews-vectors-negative300.bin.gz", binary=True)
    wv.init_sims(replace=True)

    print("Embedding data ...")
    X_train_word_average = averaging_docs(wv, data)

    df_train = pd.DataFrame(data=X_train_word_average)
    df_train["topic"] = label
    df_train.to_csv(args["data"][:-4] + '_embedded_.csv', index=False, sep=',')

    print("Feature space saved!")
    print("Done!")
