# Machine Learning Coding Challenge #

In short, I divide this coding assessment into two sections. First, a multi-class classification task using original labels. Second, I label both the training set and testing set in an unsupervised scheme. To evaluate the second section, I run again a multi-class classification section (i.e. first section) with new labels.

### Installation ###

Prerequisites: 
- python >=3.7

Dependences:

- sklearn

- tensorflow

- timeit

- pandas

- numpy

- argparse

- spacy

- nltk

- gensim

- pickle

please update pagackes or install them.

### Python scripts ###

This repository has two scripts: FeatureExtraction.py and classification.py. To use the code, training set and testing set are fed to eatureExtraction.py, individually. The ouput of this script is the feature space of the respective set in .csv format, including labels. The classification.py script receives this feature space for the classification task. The difference between the first section and the second section is the labels of training and testing sets. To implement the first section, please use the original train and test text files and for the second section, please use the train and test files from the following links.

https://drive.google.com/file/d/1uz-gSX5wNhSAKvs7npAsS6K7TWTBNpT7/view?usp=sharing

https://drive.google.com/file/d/1qkQClLuo5iE_8g9QNlMFv8i34rhzFC1v/view?usp=sharing

Note: the code needs a Pre-trained word and phrase vectors. Here is the link:

https://drive.google.com/file/d/1vSaWRDrlZlwO44kOukkDxgsS7_QuvCX2/view?usp=sharing

### Command Line Arguments ###

Example_1:  python FeatureExtraction.py -d train_withMahmood_labels.csv

Example_2:  python FeatureExtraction.py -d test_withMahmood_labels.csv

Example_3:  python classification.py -d train_withMahmood_labels_embedded.csv -t test_withMahmood_labels_embedded.csv 


-d | -t 

        -d     -training data file in the csv format
        -t     -testing data file in the csv format

Note: for plain text, please use .txt extension

### Extra questions ###

- For evaluation, I reported accuracy, precision, recall, f1-score and confusion matrix. The baseline is the classification results using original labels. For more detail, please ready the evaluation_results file.

- I strongly believe that more analysis on the original topics will improve the performance. Additionally, given our NLP task, we can leverage a particular Bi-LSTM structure to improve the performance.

- To put the model to the product, I need to be familiar with Isentia objectives, in more detail. 

- Since my grouping scheme is unsupervised, new topics are not a big hurdle. However, we update the model by incrementally training on the new corpus. That is, we do not need to train the model from scratch. Meanwhile, our deployment strategy may raise other concerns. To have a smooth scheme for new topics and include them in production, we can use A/B testing. 

