import argparse
from timeit import default_timer as timer
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import Dropout
from sklearn.preprocessing import StandardScaler, LabelEncoder
import pandas as pd
import numpy as np
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report



print("mahmood")
from keras import utils


def data_load(file):
    with open(file, 'r') as bin1:
        data = pd.read_csv(bin1)
    bin1.close()
    # x1 = data.groupby(['topic']).size()
    # print(x1.to_string)
    label = data["topic"].values
    data.drop(["topic"], inplace=True, axis=1)

    return data.values, label


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Document Classification')
    parser.add_argument("-d", "--data", required=True, help="Data for training in csv format")
    parser.add_argument("-t", "--test", required=True, help="Data for testing in csv format")

    args = vars(parser.parse_args())

    print("Loading training set ...")
    x, y = data_load(args["data"])
    print("Loading testing set ...")
    x_te, y_te = data_load(args["test"])

    # encode class values as integers
    encoder = LabelEncoder()
    encoder.fit(y)
    Y = encoder.transform(y)
    Y_te = encoder.transform(y_te)

    # model initiation
    InpDim = x.shape[1]
    batch_size = 32
    epochs = 20
    num_classes = np.max(Y) + 1
    Y = utils.to_categorical(Y, num_classes)
    y_te = utils.to_categorical(Y_te, num_classes)
    model = Sequential()
    model.add(Dense(1024, input_shape=(InpDim,)))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    # model training
    history = model.fit(x, Y, batch_size=batch_size, epochs=epochs, verbose=1, validation_split=0.1)

    start = timer()
    # prediction on testing set
    PredictionTemp = model.predict(x_te)
    end = timer()
    print('Testing with %d samples completed in %.2f seconds' % (x_te.shape[0], end - start))

    y_pred = np.argmax(PredictionTemp, axis=1)
    print('Accuracy is %.2f' % (100 * accuracy_score(Y_te, y_pred)))
    print(classification_report(Y_te, y_pred))

    print("Confusion Matrix")
    tmp = pd.DataFrame(confusion_matrix(Y_te, y_pred), index=np.unique(y), columns=np.unique(y))
    print(tmp.to_string())
